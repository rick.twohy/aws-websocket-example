#########
# Config
#########
locals {
  api_stage = "develop"

  api_url            = "wss://${aws_apigatewayv2_api.ws_example_api_gateway.id}.execute-api.${data.aws_region.current.name}.amazonaws.com/${local.api_stage}"
  api_connection_url = "https://${aws_apigatewayv2_api.ws_example_api_gateway.id}.execute-api.${data.aws_region.current.name}.amazonaws.com/${local.api_stage}"
}

data "aws_region" "current" {}

######
# IAM
######
data "aws_iam_policy_document" "ws_example_api_gateway_policy" {
  statement {
    actions = [
      "lambda:InvokeFunction",
    ]
    effect = "Allow"
    resources = [
      module.websocket_connect_function.lambda_function_arn,
      module.websocket_disconnect_function.lambda_function_arn,
      module.websocket_sendMessage_function.lambda_function_arn
    ]
  }
}

resource "aws_iam_policy" "ws_example_api_gateway_policy" {
  name   = "WsMessengerAPIGatewayPolicy"
  path   = "/"
  policy = data.aws_iam_policy_document.ws_example_api_gateway_policy.json
}

resource "aws_iam_role" "ws_example_api_gateway_role" {
  name = "WsMessengerAPIGatewayRole"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "apigateway.amazonaws.com"
        }
      },
    ]
  })

  managed_policy_arns = [
    aws_iam_policy.ws_example_api_gateway_policy.arn
  ]
}

########################
# Websocket Api Gateway
########################
resource "aws_apigatewayv2_api" "ws_example_api_gateway" {
  name                       = "atest-websocket-api-gateway"
  protocol_type              = "WEBSOCKET"
  route_selection_expression = "$request.body.action"
}

resource "aws_apigatewayv2_stage" "ws_messenger_api_stage" {
  api_id      = aws_apigatewayv2_api.ws_example_api_gateway.id
  name        = local.api_stage
  auto_deploy = true

  default_route_settings {
    // logging
    data_trace_enabled       = true
    detailed_metrics_enabled = true
    logging_level            = "INFO"

    // rate-limiting
    throttling_burst_limit = 5
    throttling_rate_limit  = 5
  }
}

# WEBSOCKET CONNECT ROUTE
resource "aws_apigatewayv2_integration" "ws_connect_integration" {
  api_id          = aws_apigatewayv2_api.ws_example_api_gateway.id
  integration_uri = module.websocket_connect_function.lambda_function_invoke_arn
  credentials_arn = aws_iam_role.ws_example_api_gateway_role.arn

  description               = "Lambda connect integration example"
  integration_type          = "AWS_PROXY"
  integration_method        = "POST"
  content_handling_strategy = "CONVERT_TO_TEXT"
  passthrough_behavior      = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_route_response" "ws_messenger_api_connect_response" {
  api_id             = aws_apigatewayv2_api.ws_example_api_gateway.id
  route_id           = aws_apigatewayv2_route.ws_messenger_api_connect_route.id
  route_response_key = "$default"
}

resource "aws_apigatewayv2_route" "ws_messenger_api_connect_route" {
  api_id             = aws_apigatewayv2_api.ws_example_api_gateway.id
  route_key          = "$connect"
  target             = "integrations/${aws_apigatewayv2_integration.ws_connect_integration.id}"
  authorization_type = "CUSTOM"
  authorizer_id      = aws_apigatewayv2_authorizer.websocket_authorizer.id
}

# WEBSOCKET DISCONNECT ROUTE
resource "aws_apigatewayv2_integration" "ws_disconnect_integration" {
  api_id          = aws_apigatewayv2_api.ws_example_api_gateway.id
  integration_uri = module.websocket_disconnect_function.lambda_function_invoke_arn
  credentials_arn = aws_iam_role.ws_example_api_gateway_role.arn

  description               = "Lambda disconnect integration example"
  integration_type          = "AWS_PROXY"
  integration_method        = "POST"
  content_handling_strategy = "CONVERT_TO_TEXT"
  passthrough_behavior      = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_route_response" "ws_messenger_api_disconnect_response" {
  api_id             = aws_apigatewayv2_api.ws_example_api_gateway.id
  route_id           = aws_apigatewayv2_route.ws_messenger_api_disconnect_route.id
  route_response_key = "$default"
}

resource "aws_apigatewayv2_route" "ws_messenger_api_disconnect_route" {
  api_id    = aws_apigatewayv2_api.ws_example_api_gateway.id
  route_key = "$disconnect"
  target    = "integrations/${aws_apigatewayv2_integration.ws_disconnect_integration.id}"
}

# WEBSOCKET SEND MESSAGE ROUTE
resource "aws_apigatewayv2_integration" "ws_sendMessage_integration" {
  api_id          = aws_apigatewayv2_api.ws_example_api_gateway.id
  integration_uri = module.websocket_sendMessage_function.lambda_function_invoke_arn
  credentials_arn = aws_iam_role.ws_example_api_gateway_role.arn

  description               = "Lambda sendMessage integration example"
  integration_type          = "AWS_PROXY"
  integration_method        = "POST"
  content_handling_strategy = "CONVERT_TO_TEXT"
  passthrough_behavior      = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_route_response" "ws_messenger_api_sendMessage_response" {
  api_id             = aws_apigatewayv2_api.ws_example_api_gateway.id
  route_id           = aws_apigatewayv2_route.ws_messenger_api_sendMessage_route.id
  route_response_key = "$default"
}

resource "aws_apigatewayv2_route" "ws_messenger_api_sendMessage_route" {
  api_id    = aws_apigatewayv2_api.ws_example_api_gateway.id
  route_key = "sendMessage"
  target    = "integrations/${aws_apigatewayv2_integration.ws_sendMessage_integration.id}"
}

##########
# Lambdas
##########
module "websocket_authorizer_function" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "~> 3.3.1"

  function_name          = "atest-websocket-authorizer-function"
  description            = "The authorizer function for the websocket"
  handler                = "authorizer.perform"
  runtime                = "nodejs18.x"
  publish                = true
  create_package         = false
  local_existing_package = join(" ", fileset(path.module, "**/deployable-authorizer*.zip"))

  allowed_triggers = {
    AllowExecutionFromAPIGateway = {
      service    = "apigateway"
      source_arn = "${aws_apigatewayv2_api.ws_example_api_gateway.execution_arn}/*/*"
    }
  }
}

module "websocket_connect_function" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "~> 3.3.1"

  function_name          = "atest-websocket-connect-function"
  description            = "The connect function for the websocket"
  handler                = "connect.perform"
  runtime                = "nodejs18.x"
  publish                = true
  create_package         = false
  local_existing_package = join(" ", fileset(path.module, "**/deployable-connect*.zip"))

  environment_variables = {
    WEBSOCKET_TABLE = aws_dynamodb_table.websocket_connections.id
  }

  allowed_triggers = {
    AllowExecutionFromAPIGateway = {
      service    = "apigateway"
      source_arn = "${aws_apigatewayv2_api.ws_example_api_gateway.execution_arn}/*/*"
    }
  }

  attach_policy_statements = true
  policy_statements = {
    allow_dynamo_websocket_connections = {
      effect  = "Allow",
      actions = ["dynamodb:*"],
      resources = [
        resource.aws_dynamodb_table.websocket_connections.arn,
        "${resource.aws_dynamodb_table.websocket_connections.arn}/*",
      ]
    }
  }

  # layers = [
  #   module.websocket_authorizer_function.lambda_layer_arn
  # ]
}

module "websocket_disconnect_function" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "~> 3.3.1"

  function_name          = "atest-websocket-disconnect-function"
  description            = "The disconnect function for the websocket"
  handler                = "disconnect.perform"
  runtime                = "nodejs18.x"
  publish                = true
  create_package         = false
  local_existing_package = join(" ", fileset(path.module, "**/deployable-disconnect*.zip"))

  environment_variables = {
    WEBSOCKET_TABLE = aws_dynamodb_table.websocket_connections.id
  }

  allowed_triggers = {
    AllowExecutionFromAPIGateway = {
      service    = "apigateway"
      source_arn = "${aws_apigatewayv2_api.ws_example_api_gateway.execution_arn}/*/*"
    }
  }

  attach_policy_statements = true
  policy_statements = {
    allow_dynamo_websocket_connections = {
      effect  = "Allow",
      actions = ["dynamodb:*"],
      resources = [
        resource.aws_dynamodb_table.websocket_connections.arn,
        "${resource.aws_dynamodb_table.websocket_connections.arn}/*",
      ]
    }
  }
}

module "websocket_sendMessage_function" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "~> 3.3.1"

  function_name          = "atest-websocket-sendMessage-function"
  description            = "The sendMessage function for the websocket"
  handler                = "sendMessage.perform"
  runtime                = "nodejs18.x"
  publish                = true
  create_package         = false
  local_existing_package = join(" ", fileset(path.module, "**/deployable-sendMessage*.zip"))

  environment_variables = {
    WEBSOCKET_CONNECTION_URL = local.api_connection_url
    WEBSOCKET_TABLE          = aws_dynamodb_table.websocket_connections.id
  }

  allowed_triggers = {
    AllowExecutionFromAPIGateway = {
      service    = "apigateway"
      source_arn = "${aws_apigatewayv2_api.ws_example_api_gateway.execution_arn}/*/*"
    }
  }

  attach_policy_statements = true
  policy_statements = {
    allow_dynamo_websocket_connections = {
      effect  = "Allow",
      actions = ["dynamodb:*"],
      resources = [
        resource.aws_dynamodb_table.websocket_connections.arn,
        "${resource.aws_dynamodb_table.websocket_connections.arn}/*",
      ]
    }

    allow_manage_websocket_connections = {
      effect  = "Allow",
      actions = ["execute-api:ManageConnections"],
      resources = [
        "${aws_apigatewayv2_api.ws_example_api_gateway.execution_arn}/*/*"
      ]
    }
  }
}

module "websocket_publish_function" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "~> 3.3.1"

  function_name          = "atest-websocket-publish-function"
  description            = "The publish function that pushes to all connected clients"
  handler                = "publish.perform"
  runtime                = "nodejs18.x"
  publish                = true
  create_package         = false
  local_existing_package = join(" ", fileset(path.module, "**/deployable-publish*.zip"))

  environment_variables = {
    WEBSOCKET_CONNECTION_URL = local.api_connection_url
    WEBSOCKET_TABLE = aws_dynamodb_table.websocket_connections.id
  }

  allowed_triggers = {
    AllowExecutionFromAPIGateway = {
      service    = "apigateway"
      source_arn = "${aws_apigatewayv2_api.ws_example_api_gateway.execution_arn}/*/*"
    }
  }

  attach_policy_statements = true
  policy_statements = {
    allow_dynamo_websocket_connections = {
      effect  = "Allow",
      actions = ["dynamodb:*"],
      resources = [
        resource.aws_dynamodb_table.websocket_connections.arn,
        "${resource.aws_dynamodb_table.websocket_connections.arn}/*",
      ]
    }

    allow_manage_websocket_connections = {
      effect  = "Allow",
      actions = ["execute-api:ManageConnections"],
      resources = [
        "${aws_apigatewayv2_api.ws_example_api_gateway.execution_arn}/*/*"
      ]
    }
  }
}

############
# Dynamo DB
############
resource "aws_dynamodb_table" "websocket_connections" {
  name           = "websocket-connections"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "connectionId"

  attribute {
    name = "connectionId"
    type = "S"
  }
}

############
# Auth
############
resource "aws_apigatewayv2_authorizer" "websocket_authorizer" {
  api_id          = aws_apigatewayv2_api.ws_example_api_gateway.id
  authorizer_type = "REQUEST"
  authorizer_uri  = module.websocket_authorizer_function.lambda_function_invoke_arn
  identity_sources = ["route.request.header.Authorization"]
  name             = "example-websocket-authorizer"
}

resource "aws_iam_role" "websocket_authorizer_role" {
  name = "websocket-authorizer-role"
  assume_role_policy = jsonencode({
    "Version" = "2012-10-17"
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          Service = "apigateway.amazonaws.com"
        },
        Action = "sts:AssumeRole"
      }
    ]
  })
  inline_policy {
    name = "websocket-authorizer-policy"
    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Effect = "Allow"
          Action = [
            "lambda:invokeFunction"
          ]
          Resource = "*"
        }
      ]
    })
  }
}

############
# Event Bridge
############
resource "aws_cloudwatch_event_rule" "publish_to_websocket_event_rule" {
  name                = "publish-to-websocket-event-rule"
  description         = "retry scheduled every 1 minute"
  schedule_expression = "rate(1 minute)"
}

resource "aws_cloudwatch_event_target" "publish_lambda_target" {
  arn  = module.websocket_publish_function.lambda_function_arn
  rule = aws_cloudwatch_event_rule.publish_to_websocket_event_rule.name
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_publish_lambda" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = module.websocket_publish_function.lambda_function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.publish_to_websocket_event_rule.arn
}

############
# Outputs
############
output "websocket_url" {
  value = aws_apigatewayv2_stage.ws_messenger_api_stage.invoke_url
}

