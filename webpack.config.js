const path = require('path');
const ZipPlugin = require('zip-webpack-plugin');
const fs = require('fs');

const srcPath = path.resolve(__dirname, './src');
const performerPath = path.resolve(`${__dirname}/src/performers`);
const distPath = path.resolve(__dirname, './dist');

const performerEntries = {};
fs.readdirSync(performerPath).forEach((str) => {
	performerEntries[str.split(/(\.[jt]s)/g)[0]] = `${performerPath}/${str}`;
});

module.exports = {
	entry: performerEntries,
	mode: 'production',
	output: {
		path: distPath,
		filename: '[name].js',
		library: 'index',
		libraryTarget: 'umd',
		clean: true
	},
	devtool: 'source-map',
	plugins: [
		...Object.keys(performerEntries).map((entry) => new ZipPlugin({
			include: [`${entry}.js`],
			path: distPath,
			filename: `deployable-${entry}.zip`
		}))
	],
	module: {
		rules: [
			{
				test: /\.(js|mjs|ts)$/,
				include: srcPath,
				exclude: path.resolve(__dirname, '/node_modules/'),
				loader: require.resolve('babel-loader'),
				options: {
					cacheDirectory: true,
					cacheCompression: false,
				}
			}
		]
	},
	target: 'node',
	optimization: {
		minimize: false
	},
	cache: {
		type: 'filesystem'
	},
	externalsPresets: {
		node: true
	}
};
