import { DeleteItemCommand, DynamoDBClient } from "@aws-sdk/client-dynamodb";

export const perform = async (event: any) => {
    console.log("DISCONNECT EVENT", event)

    const connectionId = event.requestContext.connectionId;

    await dropConnectionFromDynamo({connectionId})

    return {
        statusCode: 200
    }
}

const dropConnectionFromDynamo = async ({connectionId}: {connectionId: string}) => {
    const client = new DynamoDBClient({})

    const command = new DeleteItemCommand({
        TableName: process.env.WEBSOCKET_TABLE,
        Key: {
            connectionId: {
                S: connectionId
            } 
        }
    })

    await client.send(command)
}