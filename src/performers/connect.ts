import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { PutCommand } from "@aws-sdk/lib-dynamodb";

export const perform = async (event: any) => {
    console.log("CONNECT EVENT", event)

    const idpId = event.queryStringParameters.idpId;
    const { connectionId, appId } = event.requestContext;

    await addConnectionToDynamo({ idpId, appId, connectionId })

    return {
        statusCode: 200,
    }
}

const addConnectionToDynamo = async ({
    appId, connectionId, idpId
}: {
    appId: string, 
    connectionId: string, 
    idpId: string
}) => {
    const client = new DynamoDBClient()

    const command = new PutCommand({
        TableName: process.env.WEBSOCKET_TABLE,
        Item: {
            idpId,
            connectionId,
            appId
        }
    })

    await client.send(command)
}
