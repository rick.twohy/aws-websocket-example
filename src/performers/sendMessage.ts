import { ApiGatewayManagementApiClient, PostToConnectionCommand } from "@aws-sdk/client-apigatewaymanagementapi"
import { DynamoDBClient } from "@aws-sdk/client-dynamodb"
import { GetCommand } from "@aws-sdk/lib-dynamodb"

export const perform = async (event: any) => {
    console.log("SEND MESSAGE EVENT", event)

    const connectionId = event.requestContext.connectionId

    const connectedUser = await lookupConnectionInDynamo(connectionId)

    const body = JSON.parse(event.body)

    const client = new ApiGatewayManagementApiClient({
        endpoint: process.env.WEBSOCKET_CONNECTION_URL
    })
    const command = new PostToConnectionCommand({
        ConnectionId: connectionId,
        Data: `responding to ${connectionId} for user ${connectedUser?.idpId}. Here is the input BTW ${body.message}`,
    })
    await client.send(command)

    return {
        statusCode: 200
    };
}

const lookupConnectionInDynamo = async (connectionId: string) => {
    const client = new DynamoDBClient()

    const command = new GetCommand({
        TableName: process.env.WEBSOCKET_TABLE,
        Key: {
            "connectionId": connectionId
        }
    })

    return (await client.send(command)).Item
}