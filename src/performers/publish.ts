import { ApiGatewayManagementApiClient, PostToConnectionCommand } from "@aws-sdk/client-apigatewaymanagementapi";
import { DynamoDBClient, ScanCommand } from "@aws-sdk/client-dynamodb";
import { unmarshall } from "@aws-sdk/util-dynamodb";

export const perform = async (event: any, context: any, callback: any) => {
    console.log("PUBLISH EVENT", event)
    
    const connections = await getAllConnections() || []

    await Promise.all(
        connections.map(async (item)=>{
            const connection =  unmarshall(item)
            console.log(`Notifying ${connection}`)
            await notifyConnection(connection.connectionId, connection.idpId)
            console.log(`Notified ${connection}`)
        })
    )

    return {
        statusCode: 200,
    };
}

const notifyConnection = async (connectionId: string, idpId: string) => {
    const client = new ApiGatewayManagementApiClient({
        endpoint: process.env.WEBSOCKET_CONNECTION_URL
    })
    const command = new PostToConnectionCommand({
        ConnectionId: connectionId,
        Data: `Publishing to ${connectionId} for user ${idpId}. Hello from our publish system!`,
    })
    await client.send(command)
}

const getAllConnections = async () => {
    const client = new DynamoDBClient()

    const command = new ScanCommand({
        TableName: process.env.WEBSOCKET_TABLE
    })

   const response =  await client.send(command)
   return response.Items
}