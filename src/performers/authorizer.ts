type AuthResponse = {
    principalId: string
    context: any
    policyDocument: {
        Version?: string
        Statement?: {
            Action: string,
            Effect: string,
            Resource: string
        }[]
    }
}
export const perform = async (event: any, context: any, callback: any) => {
    console.log('Received event:', JSON.stringify(event, null, 2));

    // only check to see if the request has authorization as a header.  Super simple!
   if(event.headers.Authorization) {
        return generatePolicy('me', 'Allow', event.methodArn)
    } else {
        return generatePolicy('me', 'Deny', event.methodArn)
    }
}

// Helper function to generate an IAM policy
var generatePolicy = function(principalId: string, effect: string, resource: string) {
    var authResponse: AuthResponse = {
        principalId,
        policyDocument: {},
        context: {}
    }; 

   // Required output:
   if (effect && resource) {
        authResponse.policyDocument.Version = '2012-10-17'; // default version
        authResponse.policyDocument.Statement = [
            {
                Action: 'execute-api:Invoke',
                Effect: effect,
                Resource: resource
            }
        ];
    }
   // Optional output with custom properties of the String, Number or Boolean type.
   authResponse.context = {
       "stringKey": "stringval",
       "numberKey": 123,
       "booleanKey": true
    };

    console.log("RESPONSE", JSON.stringify(authResponse))
   return authResponse;
}