

## Setup

```npm
npm install
npm run build
terraform init
terraform apply --auto-approve
```

## Resources

- Api Gateway Websocket
- Lambdas
  - authorize 		(used to authorize the client into the websocket)
  - connect 			(connection route for websocket that adds the client to dynamo)
  - disconnect		(disconnection route for websocket that removes the client from dynamo)
  - sendMessage 		(used to interact with the websocket from the client)
  - publish		 	(every minute a publish message gets sent out to every connected client from event bridge)
- Event Bridge
  - publish every 1 minute
- Dynamo Table
  - websocket-connections
    - tracks all connected clients

## Usage

- Grab the websocket url from the output of the terraform apply

  - `websocket_url = "wss://<api-id>.execute-api.us-east-1.amazonaws.com/develop"`
- Use a websocket client that accepts headers

  - postman
  - wscat
  - etc...
- Connect to the `websocket-url` using this url and header

  - url: `wss://<api-id>.execute-api.us-east-1.amazonaws.com/develop?idpId=<any-id>`
  - Authorization:`<any-value>`
- Once connected you will recieve a published message every 1 minute from event bridge.

  - Example: `Publishing to <connection-id> for user <idpId>. Hello from our publish system!`
- You can send messages to the websocket using the `sendMessage` action.

  - Example Request: `{"action":"sendMessage", "message": "hello from the websocket!"}`
  - Example Response: `responding to <connection-id> for user <idpId>. Here is the input BTW hello from the websocket!`
