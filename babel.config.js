module.exports = {
	presets: [
		'@babel/preset-typescript',
		['@babel/preset-env', {
			"targets":{
				node: 'current'
			}
		}],
	],
	plugins: [
		"@babel/plugin-proposal-function-bind",
		"@babel/plugin-transform-arrow-functions",
	]
}
